package com.connor.sqs.example;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SQSBatchResponse;
import com.amazonaws.services.lambda.runtime.events.SQSBatchResponse.BatchItemFailure;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;
import com.amazonaws.services.lambda.runtime.events.SQSEvent.SQSMessage;

import java.util.ArrayList;
import java.util.List;

public class RequestHandlerSqsImpl implements RequestHandler<SQSEvent, SQSBatchResponse> {
    @Override
    public SQSBatchResponse handleRequest(final SQSEvent input, final Context context) {
        List<SQSBatchResponse.BatchItemFailure> failures = new ArrayList<>();
        for (SQSMessage sqsMessage : input.getRecords()) {
            try {
                processMessageOrThrow(sqsMessage);
            } catch (Exception e) {
                failures.add(new BatchItemFailure(sqsMessage.getMessageId()));
                e.printStackTrace(); //could add to x-ray here too, but structured logging should be enough
            }
        }
        return new SQSBatchResponse(failures);
    }

    private void processMessageOrThrow(final SQSMessage sqsMessage) {
        if (Integer.parseInt(sqsMessage.getBody()) == 1) {
            final String errorMessage = String.format("Error processing message with id '%s' and body '%s'", sqsMessage.getMessageId(), sqsMessage.getBody());
            throw new RuntimeException(errorMessage);
        }
    }
}