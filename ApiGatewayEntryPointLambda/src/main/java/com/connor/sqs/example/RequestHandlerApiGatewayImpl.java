package com.connor.sqs.example;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.SendMessageBatchRequest;
import com.amazonaws.services.sqs.model.SendMessageBatchRequestEntry;
import com.amazonaws.services.sqs.model.SendMessageBatchResult;

import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class RequestHandlerApiGatewayImpl implements RequestHandler<APIGatewayProxyRequestEvent,
        APIGatewayProxyResponseEvent> {
    private static final int MAX_ALLOWED_NUMBER_OF_SQS_MESSAGES_IN_BATCH = 10;
    private static final String QUEUE_URL = System.getenv("QUEUE_URL");
    private static final String QUEUE_TWO_URL = System.getenv("QUEUE_TWO_URL");
    private final AmazonSQS amazonSQS;

    public RequestHandlerApiGatewayImpl(AmazonSQS amazonSQS) {
        this.amazonSQS = amazonSQS;
    }

    public RequestHandlerApiGatewayImpl() {
        this(AmazonSQSClientBuilder.defaultClient());
    }

    public APIGatewayProxyResponseEvent handleRequest(final APIGatewayProxyRequestEvent input, final Context context) {
        final SendMessageBatchRequest sendMessageBatchRequestForQueueBeingPropertlyHandled = new SendMessageBatchRequest()
                .withQueueUrl(QUEUE_URL)
                .withEntries(getSqsBatchEntries());
        final SendMessageBatchResult sendMessageBatchResult = amazonSQS.sendMessageBatch(sendMessageBatchRequestForQueueBeingPropertlyHandled);
        if(!sendMessageBatchResult.getFailed().isEmpty()){
            //NOTE: in the real world, you would do something else here, this should never happen for our simple use
            //case, so just throw
            throw new RuntimeException("Some entries in the batch failed to send");
        }
        System.out.printf("Successfully sent batch of messages to first sqs '%s'%n",
                sendMessageBatchRequestForQueueBeingPropertlyHandled);

        final SendMessageBatchRequest sendMessageBatchRequestForQueueNotProperlyHandled = new SendMessageBatchRequest()
                .withQueueUrl(QUEUE_TWO_URL)
                .withEntries(getSqsBatchEntries());
        final SendMessageBatchResult sendMessageBatchResultForBadQueue =
                amazonSQS.sendMessageBatch(sendMessageBatchRequestForQueueNotProperlyHandled);
        if(!sendMessageBatchResultForBadQueue.getFailed().isEmpty()){
            //NOTE: in the real world, you would do something else here, this should never happen for our simple use
            //case, so just throw
            throw new RuntimeException("Some entries in the batch failed to send");
        }
        return new APIGatewayProxyResponseEvent()
                .withStatusCode(200)
                .withBody("{}");
    }

    private Collection<SendMessageBatchRequestEntry> getSqsBatchEntries(){
        final int endIndexExclusive = MAX_ALLOWED_NUMBER_OF_SQS_MESSAGES_IN_BATCH + 1;
        return IntStream.range(1, endIndexExclusive)
                .mapToObj(String::valueOf)
                .map(intAsString -> new SendMessageBatchRequestEntry().withId(UUID.randomUUID().toString()).withMessageBody(intAsString))
                .collect(Collectors.toList());
    }
}
