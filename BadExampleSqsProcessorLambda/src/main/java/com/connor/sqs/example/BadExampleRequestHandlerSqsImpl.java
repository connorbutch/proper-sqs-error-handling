package com.connor.sqs.example;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SQSBatchResponse;
import com.amazonaws.services.lambda.runtime.events.SQSBatchResponse.BatchItemFailure;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;
import com.amazonaws.services.lambda.runtime.events.SQSEvent.SQSMessage;

import java.util.ArrayList;
import java.util.List;

/**
 * DO NOT REPLICATE THIS CLASS -- IT SHOWS A BAD WAY OF HANDLING ERRORS WITHIN SQS
 */
public class BadExampleRequestHandlerSqsImpl implements RequestHandler<SQSEvent, Void> {
    @Override
    public Void handleRequest(final SQSEvent input, final Context context) {
        for (SQSMessage sqsMessage : input.getRecords()) {
            processMessageOrThrow(sqsMessage);
        }
        return null;
    }

    private void processMessageOrThrow(final SQSMessage sqsMessage) {
        if (Integer.parseInt(sqsMessage.getBody()) == 1) {
            final String errorMessage = String.format("Error processing message with id '%s' and body '%s'",
                    sqsMessage.getMessageId(), sqsMessage.getBody());
            throw new RuntimeException(errorMessage);
        }
    }
}
